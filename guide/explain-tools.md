# 1. [↖回到主目录](https://gitee.com/topfox/topfox/blob/dev/README.md)

# 2. 上下文对象 AppContext

## 2.1. getBean(String name)
- 静态方法, 根据指定的名称从Ioc容器中获得Bean

## 2.2. getBean(Class<T> clazz)
- 静态方法, 根据指定的Class 从Ioc容器中获得Bean

## 2.3. getBean(String name,Class<T> clazz)
- 静态方法, 根据指定的名称和Class 从Ioc容器中获得Bean

## 2.4. getRestSessionHandler()
- 静态方法, 返回接口对象 IRestSessionHandler
- 可强制转化为 com.topfox.util.RestSessionHandler 对象, 或者RestSessionHandler的子类.

## 2.5. getAbstractRestSession()
- 静态方法, 返回 com.topfox.common.AbstractRestSession对象
- 可强制转换为AbstractRestSession的子类

## 2.6. getSysConfig()
静态方法,  获得TopFox的参数对象

## 2.7. environment()
静态方法, 获得 org.springframework.core.env.Environment 对象


# 3. 上下文对象 AppContext 如何使用
下面源码中的 RestSession和RestSessionConfig对象可以参考 <<快速使用>>章节中的相关内容

AppContext 提供了几个静态方法, 直接获取相关对象.

```java
package com.user.controller;

import com.topfox.annotation.TokenOff;

import com.sys.RestSession;
import com.sys.RestSessionConfig;
import com.topfox.common.AppContext;
import com.topfox.common.SysConfigRead;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/context")
public class AppContextController {

    /**
     * AppContext.getRestSessionHandler()是同一个实例
     */
    @Autowired RestSessionConfig restSessionConfig;

    @TokenOff
    @GetMapping("/test1")
    public void test1() {
        Environment environment = AppContext.environment();
        RestSessionConfig restSessionHandlerConfig = (RestSessionConfig)AppContext.getRestSessionHandler();

        //与 restSessionConfig.get()的获得的对象一样
        RestSession restSession = (RestSession)AppContext.getAbstractRestSession();


        SysConfigRead configRead = AppContext.getSysConfig();
        System.out.println(configRead);
    }

    @TokenOff
    @GetMapping("/test2")
    public void test2() {
        RestSession restSession = restSessionConfig.get();
        SysConfigRead configRead = restSessionConfig.getSysConfig();
    }
}
```
